import QtQuick 2.15
import QtQuick.Controls 2.15

Page {
    id: pageRoot
    anchors.fill: parent

    width: 400
    height: 400

    title: qsTr("Devices")

    property var devicesArray
    property var selectedDevice

    onVisibleChanged: {
        if (pageRoot.visible) {
            parseDevices()
        }
    }


    function parseDevices() {
        label.text = "Подключение к серверу"
        devicesList.visible = false
        label.visible = true
        var req = new XMLHttpRequest()

        var timer = Qt.createQmlObject("import QtQuick 2.3; Timer {interval: 4000; repeat: false; running: true;}",root,"MyTimer")
        timer.triggered.connect(function(){
            req.abort()
            processDevices("timedout")
        });

        var url = "http://" + globals.server + "/api/client/devices/list/" + globals.token
        req.open("GET", url, true)
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
        req.setRequestHeader("Connection", "close");
        req.onreadystatechange = function() {
            if (req.readyState === XMLHttpRequest.DONE){
                if (req.status == 200) {
                    timer.running = false
                    processDevices(req.responseText)
                } else {
                    processDevices(req.status)
                }
            }
        }
        console.log(url)
        req.send();
    }

    function processDevices(status) {
        console.log(status)
        if (status.length > 15) {
            console.log("Good")
            var data = JSON.parse(status);
            devicesArray = data
            devices.clear()
            for (var i in data) {
                devices.append({num: i, name: data[i]["name"]})
            }
            label.visible = false
            devicesList.visible = true
        } else if (status.includes("[]")) {
            label.text = "У вас нет устройств"
            console.log("Zero devices")
        } else if (status == "Bad auth data") {
            rootLabel.text = "Токен недействителен\nПовторите авторизацию"
            rootButton.visible = true
            loginWindow.show()
            homeWindow.hide()
            console.log("Timed out")
        } else if (status == "timedout") {
            rootLabel.text = "Невозможно подключиться к серверу\nПроверьте подключение к интернету"
            rootButton.visible = true
            root.show()
            homeWindow.hide()
            console.log("Timed out")
        } else {
            rootLabel.text = "Неизвестная ошибка\nСообщите о ней компании\n" + status
            rootButton.visible = true
            root.show()
            homeWindow.hide()
            console.log("Unknown error")
            console.log(status)
        }
    }

    ScrollView {
        id: devicesList

        anchors.fill: parent

        visible: false

        ListView {
            id: listView
            width: parent.width

            Component {
                id: delegateComponent
                ItemDelegate {
                    width: parent.width
                    text: name
                    onClicked: {
                        selectedDevice = num
                        stackView.push(devicesInfoPage)
                    }
                }
            }


            ListModel {
                id: devices
            }

            model: devices


            delegate: delegateComponent
        }
    }

    Label {
        id: label
        text: qsTr("Подключение к серверу")
        anchors.centerIn: parent
    }
}

