import QtQuick 2.9
import QtQuick.Controls 2.5

Page {
    anchors.fill: parent

    width: 400
    height: 400

    title: qsTr("Home")

    Label {
        text: qsTr("Вы на домашней странице\nОткройте меню, нажав на кнопку в верхнем левом углу")
        anchors.centerIn: parent
    }
}

