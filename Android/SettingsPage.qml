import QtQuick 2.9
import QtQuick.Controls 2.5

Page {
    anchors.fill: parent

    width: 400
    height: 400

    title: qsTr("Settings")

    Label {
        text: qsTr("You are on the settings page.")
        anchors.centerIn: parent
    }
}

