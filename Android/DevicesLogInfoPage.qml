import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.0


Page {
    id: pageRoot
    anchors.fill: parent

    width: 400
    height: 400

    title: qsTr("Device log info")

    onVisibleChanged: {
        if (pageRoot.visible) {
            fillData()
        }
    }

    function timeConverter(UNIX_timestamp){
        var a = new Date(UNIX_timestamp * 1000);
        var months = ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'];
        var year = a.getFullYear();
        var month = months[a.getMonth()];
        var date = a.getDate();
        var hour = a.getHours();
        var min = a.getMinutes();
        var sec = a.getSeconds();
        var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
        return time;
    }

    function fillData() {
        logDataLabel.text = "Содержание лога:\n" + devicesLogsPage.logArray[devicesLogsPage.selectedLog]["logdata"]
        logTime.text = "Время добавления лога:\n" + timeConverter(devicesLogsPage.logArray[devicesLogsPage.selectedLog]["timestamp"])
    }

    ColumnLayout {
        anchors.fill: parent
        Label {
            id: logDataLabel
            text: qsTr("Содержание лога")
            Layout.alignment: Qt.AlignCenter
            horizontalAlignment: Text.AlignVCenter
        }

        Label {
            id: logTime
            text: qsTr("Время добавления лога")
            Layout.alignment: Qt.AlignCenter
            horizontalAlignment: Text.AlignVCenter
        }
    }
}
