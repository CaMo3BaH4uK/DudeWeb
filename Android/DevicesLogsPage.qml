import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.0

Page {
    id: pageRoot
    anchors.fill: parent

    width: 400
    height: 400

    title: qsTr("Device logs")

    property var selectedLog
    property var logArray

    onVisibleChanged: {
        if (pageRoot.visible) {
            parseLogs()
        }
    }

    function parseLogs() {
        label.text = "Подключение к серверу"
        logsList.visible = false
        label.visible = true
        var req = new XMLHttpRequest()

        var timer = Qt.createQmlObject("import QtQuick 2.3; Timer {interval: 4000; repeat: false; running: true;}",root,"MyTimer")
        timer.triggered.connect(function(){
            req.abort()
            processLogs("timedout")
        });

        var url = "http://" + globals.server + "/api/client/devices/logs/" + globals.token + "/" + devicesPage.devicesArray[devicesPage.selectedDevice]["deviceid"]
        req.open("GET", url, true)
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
        req.setRequestHeader("Connection", "close");
        req.onreadystatechange = function() {
            if (req.readyState === XMLHttpRequest.DONE){
                if (req.status == 200) {
                    timer.running = false
                    processLogs(req.responseText)
                } else {
                    processLogs(req.status)
                }
            }
        }
        console.log(url)
        req.send();
    }

    function processLogs(status) {
        console.log(status)
        if (status.length > 15) {
            console.log("Good")
            var data = JSON.parse(status);
            logs.clear()
            for (var i in data) {
                logs.append({num: i, logdata: data[i]["logdata"]})
            }
            logArray = data
            label.visible = false
            logsList.visible = true
        } else if (status.includes("[]")) {
            label.text = "Для данного устройства логов нет"
            console.log("Zero logs")
        } else if (status == "Bad auth data" || status == "Bad device") {
            rootLabel.text = "Токен недействителен\nПовторите авторизацию"
            rootButton.visible = true
            loginWindow.show()
            homeWindow.hide()
            console.log("Timed out")
        } else if (status == "timedout") {
            rootLabel.text = "Невозможно подключиться к серверу\nПроверьте подключение к интернету"
            rootButton.visible = true
            root.show()
            homeWindow.hide()
            console.log("Timed out")
        } else {
            rootLabel.text = "Неизвестная ошибка\nСообщите о ней компании\n" + status
            rootButton.visible = true
            root.show()
            homeWindow.hide()
            console.log("Unknown error")
            console.log(status)
        }
    }

    ScrollView {
        id: logsList

        anchors.fill: parent

        visible: false

        ListView {
            id: listView
            width: parent.width

            Component {
                id: delegateComponent
                ItemDelegate {
                    width: parent.width
                    text: logdata
                    onClicked: {
                        selectedLog = num
                        stackView.push(devicesLogInfoPage)
                    }
                }
            }


            ListModel {
                id: logs
            }

            model: logs


            delegate: delegateComponent
        }
    }

    Label {
        id: label
        text: qsTr("Подключение к серверу")
        anchors.centerIn: parent
    }
}
