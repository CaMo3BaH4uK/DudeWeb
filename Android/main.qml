import QtQuick 2.12
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import Qt.labs.settings 1.1

ApplicationWindow {
    id: root
    width: 640
    height: 480
    visible: true
    title: qsTr("FILLME")

    QtObject {
        id: globals

        property string server: "msk.ethernet.su:5555"
        property string token
    }


    Settings {
        id: settings

        property var globalTheme
        property string token
    }


    function checkToken(token) {
        var req = new XMLHttpRequest()

        var timer = Qt.createQmlObject("import QtQuick 2.3; Timer {interval: 4000; repeat: false; running: true;}",root,"MyTimer")
        timer.triggered.connect(function(){
            req.abort()
            processLogin("timedout")
        });

        var url = "http://" + globals.server + "/api/client/check/" + token
        req.open("GET", url, true)
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
        req.setRequestHeader("Connection", "close");
        req.onreadystatechange = function() {
            if (req.readyState === XMLHttpRequest.DONE){
                if (req.status == 200) {
                    console.log("ok " + req.responseText)
                    timer.running = false
                    processLogin(req.responseText)
                } else {
                    console.log("error: " + req.status)
                    processLogin(req.status)
                }
            }
        }
        console.log(url)
        req.send();
    }

    function processLogin(status) {
        console.log(status)
        if (status == "OK") {
            rootLabel.text = "Проверка успешна\nПеренаправление на домашнюю страницу"
            console.log("Good")
            globals.token = settings.token
            homeWindow.show()
            root.hide()
        } else if (status == "Bad auth data") {
            rootLabel.text = "Перенаправление на авторизацию"
            loginWindow.show()
            root.hide()
            console.log("Bad auth")
        } else if (status == "timedout" || status == 0) {
            rootLabel.text = "Невозможно подключиться к серверу\nПроверьте подключение к интернету"
            rootButton.visible = true
            console.log("Timed out")
        } else {
            rootLabel.text = "Неизвестная ошибка\nСообщите о ней компании\n" + status
            rootButton.visible = true
            console.log("Unknown error")
            console.log(status)
        }
    }

    function connCheck() {
        console.log("Token")
        console.log(settings.token)
        var req = new XMLHttpRequest()

        var timer = Qt.createQmlObject("import QtQuick 2.3; Timer {interval: 4000; repeat: false; running: true;}",root,"MyTimer")
        timer.triggered.connect(function(){
            req.abort()
            processCheck("timedout")
        });

        var url = "http://" + globals.server
        req.open("GET", url, true)
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded")
        req.setRequestHeader("Connection", "close");
        req.onreadystatechange = function() {
            if (req.readyState === XMLHttpRequest.DONE){
                if (req.status == 200) {
                    timer.running = false
                    processCheck("OK")
                } else {
                    processCheck(req.status)
                }
            }
        }
        console.log(url)
        req.send();
    }

    function processCheck(status) {
        console.log(status)
        if (status == "OK") {
            console.log("Good")
            rootLabel.text = "Подключение успешно\nПроверка токена"
            if (settings.token.length == 64){
                checkToken(settings.token)
            } else {
                rootLabel.text = "Перенаправление на авторизацию"
                loginWindow.show()
                root.hide()
            }
        } else if (status == "timedout" || status == 0) {
            rootLabel.text = "Невозможно подключиться к серверу\nПроверьте подключение к интернету"
            rootButton.visible = true
            console.log("Timed out")
        } else {
            rootLabel.text = "Неизвестная ошибка\nСообщите о ней компании\n" + status
            rootButton.visible = true
            console.log("Unknown error")
            console.log(status)
        }
    }


    function selectMode(){
        rootButton.visible = false
        rootLabel.text = "Подключение к серверу"
        connCheck()
    }

    Component.onCompleted: selectMode()

    ColumnLayout {
        anchors.fill: parent

        Label {
            id: rootLabel
            text: qsTr("Подключение к серверу")
            Layout.alignment: Qt.AlignCenter
        }

        Button {
            id: rootButton
            text: qsTr("Повторить попытку")
            visible: false
            Layout.alignment: Qt.AlignCenter
            onClicked: {
                selectMode()
            }
        }
    }



    LoginWindow {
        id: loginWindow
    }

    MainWindow {
        id: homeWindow
    }
}
