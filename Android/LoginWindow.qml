import QtQuick 2.15
import QtQuick.Window 2.3
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3

ApplicationWindow  {
    id: windowRoot

    minimumHeight: 600
    minimumWidth: 600

    title: qsTr("FILLME")

    Pane {
        anchors.fill: parent

        Item {
            id: loginPage

            width: 600
            height: 600

            anchors.centerIn: parent

            Label {
                id: header

                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                }

                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                height: 0.2*parent.height

                font.family: "Helvetica"
                font.pointSize: 24

                text: "Login"
            }

            ColumnLayout {
                anchors {
                    top: header.bottom
                    bottom: parent.bottom
                    left: parent.left
                    right: parent.right
                }

                TextField {
                    id: email
                    Layout.alignment: Qt.AlignCenter
                    Layout.preferredWidth: parent.width*0.6
                    Layout.preferredHeight: 40
                    placeholderText: "Email"
                }

                TextField {
                    id: password
                    Layout.alignment: Qt.AlignCenter
                    Layout.preferredWidth: parent.width*0.6
                    Layout.preferredHeight: 40
                    echoMode: TextInput.Password
                    placeholderText: "Password"
                }

                Switch {
                    id: remember
                    Layout.alignment: Qt.AlignCenter
                    text: "Remember"
                }

                Button {
                    Layout.alignment: Qt.AlignCenter
                    text: "Login"

                    onClicked: {
                        sendLogin(email.text, password.text, "debug")
                    }
                }
            }
        }
    }

    function sendLogin(email, password, name) {
        globals.token = "loading"

        var req = new XMLHttpRequest()

        var timer = Qt.createQmlObject("import QtQuick 2.3; Timer {interval: 4000; repeat: false; running: true;}",root,"MyTimer");
        timer.triggered.connect(function(){
            req.abort()
            globals.token = "timedout"
            processLogin()
        });


        var http = new XMLHttpRequest()
        var url = "http://" + globals.server + "/api/client/auth/login";
        var params = "email=" + email + "&password=" + password + "&devicename=" + name;
        req.open("POST", url, true);
        req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        req.setRequestHeader("Content-length", params.length);
        req.setRequestHeader("Connection", "close");
        req.onreadystatechange = function() {
            if (req.readyState === XMLHttpRequest.DONE){
                if (req.status == 200) {
                    globals.token = req.responseText
                    console.log("ok " + http.responseText)
                    timer.running = false
                    processLogin()
                } else {
                    console.log("error: " + http.status)
                    globals.token = "error"
                    processLogin()
                }
            }
        }
        console.log(url)
        console.log(params)
        req.send(params);
    }

    function processLogin() {
        console.log(globals.token)
        console.log(globals.token.length)
        if (globals.token != "" && globals.token.length == 64) {
            console.log("Good")
            if (remember.position) {
                settings.token = globals.token
                console.log("Remembered")
            }
            homeWindow.show()
            loginWindow.hide()
        } else if (globals.token == "Bad auth data") {
            console.log("Bad auth")
        } else if (globals.token == "timedout") {
            console.log("Timed out")
        } else {
            console.log("Unknown error")
        }
    }
}
