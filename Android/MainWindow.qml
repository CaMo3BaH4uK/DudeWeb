import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3

ApplicationWindow  {
    id: window

    minimumHeight: 600
    minimumWidth: 600

    title: qsTr("FILLME")

    header: ToolBar {
        contentHeight: toolButton.implicitHeight

        ToolButton {
            id: toolButton
            text: stackView.depth > 1 ? "<" : ">"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            onClicked: {
                if (stackView.depth > 1) {
                    stackView.pop()
                } else {
                    drawer.open()
                }
            }
        }

        Label {
            text: stackView.currentItem.title
            anchors.centerIn: parent
        }
    }

    Drawer {
        id: drawer
        width: window.width * 0.66
        height: window.height

        Column {
            anchors.fill: parent

            ItemDelegate {
                text: qsTr("Devices")
                width: parent.width
                onClicked: {
                    stackView.push(devicesPage)
                    drawer.close()
                }
            }
            ItemDelegate {
                text: qsTr("Settings")
                width: parent.width
                onClicked: {
                    stackView.push(settingsPage)
                    drawer.close()
                }
            }
        }
    }

    StackView {
        id: stackView
        initialItem: homePage
        anchors.fill: parent
    }

    HomePage {
        id: homePage
        visible: false
    }

    DevicesPage {
        id: devicesPage
        visible: false
    }

    SettingsPage {
        id: settingsPage
        visible: false
    }

    DevicesInfoPage {
        id: devicesInfoPage
        visible: false
    }

    DevicesLogsPage {
        id: devicesLogsPage
        visible: false
    }

    DevicesLogInfoPage {
        id: devicesLogInfoPage
        visible: false
    }
}
