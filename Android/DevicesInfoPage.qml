import QtQuick 2.9
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.0


Page {
    id: pageRoot
    anchors.fill: parent

    width: 400
    height: 400

    title: qsTr("Device info")

    onVisibleChanged: {
        if (pageRoot.visible) {
            fillData()
        }
    }

    function fillData() {
        deviceNameLabel.text = "Имя устройства:\n" + devicesPage.devicesArray[devicesPage.selectedDevice]["name"]
        deviceFastaddLabel.text = "FastAdd код:\n" + devicesPage.devicesArray[devicesPage.selectedDevice]["fastadd"]
        deviceStatusLabel.text =  "Статус устройства:\n" + (Date.now()/1000 - devicesPage.devicesArray[devicesPage.selectedDevice]["timestamp"] < 300 ? "Online" : "Offline")
    }

    ColumnLayout {
        anchors.fill: parent
        Label {
            id: deviceNameLabel
            text: qsTr("Имя устройства")
            Layout.alignment: Qt.AlignCenter
            horizontalAlignment: Text.AlignVCenter
        }

        Label {
            id: deviceFastaddLabel
            text: qsTr("FastAdd код")
            Layout.alignment: Qt.AlignCenter
            horizontalAlignment: Text.AlignVCenter
        }

        Label {
            id: deviceStatusLabel
            text: qsTr("Статус устройства")
            Layout.alignment: Qt.AlignCenter
            horizontalAlignment: Text.AlignVCenter
        }

        Button {
            id: logButton
            text: qsTr("Просмотреть лог")
            Layout.alignment: Qt.AlignCenter
            onClicked: {
                stackView.push(devicesLogsPage)
            }
        }
    }
}
