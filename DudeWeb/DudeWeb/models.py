from datetime import datetime
from flask_login import UserMixin
from DudeWeb import db
from dataclasses import dataclass

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    name = db.Column(db.String(1000))

@dataclass
class Devices(db.Model):
    deviceid: int
    userid: int
    name: str
    tokens: str
    fastadd: str
    timestamp: int

    deviceid = db.Column(db.Integer, primary_key=True)
    userid = db.Column(db.Integer)
    name = db.Column(db.String(1000))
    tokens = db.Column(db.String(10000))
    fastadd = db.Column(db.String(30))
    timestamp = db.Column(db.Integer)

@dataclass
class DevicesLogs(db.Model):
    logid: int
    deviceid: int
    userid: int
    timestamp: int
    logdata: str
    logid = db.Column(db.Integer, primary_key=True)
    deviceid = db.Column(db.Integer)
    userid = db.Column(db.Integer)
    timestamp = db.Column(db.Integer)
    logdata = db.Column(db.String(10000))

class Clients(db.Model):
    ClientID = db.Column(db.Integer, primary_key=True)
    id = db.Column(db.Integer)
    token = db.Column(db.String(10000))
    devicename = db.Column(db.String(1000))
    timestamp = db.Column(db.Integer)
    