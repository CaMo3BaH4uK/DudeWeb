"""
The flask application package.
"""

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from datetime import datetime

db = SQLAlchemy()

def timectime(s):
    return datetime.utcfromtimestamp(int(s)).strftime('%Y-%m-%d %H:%M:%S')

def createApp():
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
    app.config['SECRET_KEY'] = '116E2F91B41949F4B66E3D45A52D6BAC'
    app.jinja_env.filters['ctime'] = timectime

    db.init_app(app)

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .views import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .api import api as api_blueprint
    app.register_blueprint(api_blueprint)

    from .devices import devices as devices_blueprint
    from .devices import getOnlineStatus
    app.register_blueprint(devices_blueprint)
    app.jinja_env.filters['deviceStatus'] = getOnlineStatus

    from .models import User
    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    return app

import DudeWeb.views