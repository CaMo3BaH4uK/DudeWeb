from datetime import datetime
from flask import render_template, Blueprint, redirect, url_for, request, flash
from flask_login import login_required, current_user
from DudeWeb import db
from .models import Devices, DevicesLogs
import random, string, time

devices = Blueprint('devices', __name__)

def getOnlineStatus(s):
    return "Online" if time.time()-s < 300 else "Offline"

@devices.route('/devices')
@login_required
def index():
    currentPageTitle = "Devices"
    devices_array = Devices.query.filter_by(userid=current_user.id).all()
    return render_template('devices.html', currentPageTitle=currentPageTitle, devices_array=devices_array)

@devices.route('/devices/log/<id>')
@login_required
def log(id):
    deviceid = int(id)
    currentPageTitle = "Device log"
    device = Devices.query.filter_by(deviceid=deviceid).first()
    if not device:
        flash('Bad device id')
        return redirect(url_for('devices.index'))
    if current_user.id != device.userid:
        flash('This is not your device')
        return redirect(url_for('devices.index'))
    deviceslogs_array = DevicesLogs.query.filter_by(deviceid=deviceid).all()[::-1]

    return render_template('deviceslogs.html', currentPageTitle=currentPageTitle, deviceid=deviceid, deviceslogs_array=deviceslogs_array)


@devices.route('/devices/add')
@login_required
def add():
    currentPageTitle = "Device add"
    return render_template('adddevice.html', currentPageTitle=currentPageTitle)

@devices.route('/devices/add', methods=['POST'])
@login_required
def addPost():
    name = str(request.form.get('name'))

    if not len(name):
        flash('Enter device name')
        return redirect(url_for('devices.add'))

    token =''.join(random.choices(string.ascii_uppercase + string.digits, k=32))
    fast_add_number = str(current_user.id) + str(random.randint(100000,999999))

    new_device = Devices(userid=current_user.id, name=name, tokens=token, fastadd=fast_add_number, timestamp=int(time.time()))

    db.session.add(new_device)
    db.session.commit()

    return redirect(url_for('devices.index'))

@devices.route('/devices/delete/<id>')
@login_required
def delete(id):
    if int(id) < 1:
        flash('Bad device id')
        return redirect(url_for('devices.index'))
    currentPageTitle = "Device delete"
    return render_template('deletedevice.html', currentPageTitle=currentPageTitle, deviceid=id)

@devices.route('/devices/delete', methods=['POST'])
@login_required
def deletePost():
    deviceid = int(request.form.get('deviceid'))
    if deviceid < 1:
        flash('Bad device id')
        return redirect(url_for('devices.index'))
    device = Devices.query.filter_by(deviceid=deviceid).first()
    if not device:
        flash('Bad device id')
        return redirect(url_for('devices.index'))
    if current_user.id != device.userid:
        flash('This is not your device')
        return redirect(url_for('devices.index'))
    deviceLogs = DevicesLogs.query.filter_by(deviceid=deviceid).all()
    for log in deviceLogs:
        db.session.delete(log)
    db.session.delete(device)
    db.session.commit()
    return redirect(url_for('devices.index'))


@devices.route('/devices/fastadd/<id>')
@login_required
def fastadd(id):
    if int(id) < 1:
        flash('Bad device id')
        return redirect(url_for('devices.index'))
    currentPageTitle = "New device fastadd"
    return render_template('fastadd.html', currentPageTitle=currentPageTitle, deviceid=id)

@devices.route('/devices/fastadd', methods=['POST'])
@login_required
def fastaddPost():
    deviceid = int(request.form.get('deviceid'))
    if deviceid < 1:
        flash('Bad device id')
        return redirect(url_for('devices.index'))
    device = Devices.query.filter_by(deviceid=deviceid).first()
    if not device:
        flash('Bad device id')
        return redirect(url_for('devices.index'))
    if current_user.id != device.userid:
        flash('This is not your device')
        return redirect(url_for('devices.index'))
    
    device.fastadd = str(current_user.id) + str(random.randint(100000,999999))
    db.session.commit()
    return redirect(url_for('devices.index'))


@devices.route('/devices/edit/<id>')
@login_required
def edit(id):
    if int(id) < 1:
        flash('Bad device id')
        return redirect(url_for('devices.index'))
    currentPageTitle = "Device edit"
    device = Devices.query.filter_by(deviceid=id).first()
    return render_template('editdevice.html', currentPageTitle=currentPageTitle, deviceid=id, devicename=device.name)

@devices.route('/devices/edit', methods=['POST'])
@login_required
def editPost():
    deviceid = int(request.form.get('deviceid'))
    name = str(request.form.get('name'))
    if deviceid < 1:
        flash('Bad device id')
        return redirect(url_for('devices.index'))
    device = Devices.query.filter_by(deviceid=deviceid).first()
    if not device:
        flash('Bad device id')
        return redirect(url_for('devices.index'))
    if current_user.id != device.userid:
        flash('This is not your device')
        return redirect(url_for('devices.index'))
    device.name = name
    db.session.commit()
    return redirect(url_for('devices.index'))