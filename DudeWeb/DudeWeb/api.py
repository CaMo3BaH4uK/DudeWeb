import random, string, time
from flask import render_template, Blueprint, redirect, url_for, request, flash, jsonify
from flask_login import login_user, logout_user, login_required
from werkzeug.security import generate_password_hash, check_password_hash
from DudeWeb import db
from .models import User, Devices, DevicesLogs, Clients

api = Blueprint('api', __name__)

@api.route('/api')
def apiIndex():
    return "Use API"

@api.route('/api/get/token/<fastadd>')
def getToken(fastadd):
    fastadd = int(fastadd)

    time.sleep(0.5) #Anti bruteforce

    if fastadd < 0 or 'used' in str(fastadd):
        return "Bad fastadd"

    device = Devices.query.filter_by(fastadd=fastadd).first()
    if not device:
        return "Bad fastadd"

    device.fastadd = 'used'
    db.session.commit()

    return str(device.deviceid) + ':' + str(device.tokens)

@api.route('/api/get/name/<id>/<token>')
def getName(id, token):
    deviceid = int(id)

    if deviceid < 0:
        return "Bad deviceid"

    if len(token) != 32:
        return "Bad token"

    device = Devices.query.filter_by(deviceid=deviceid).first()
    if not device:
        return "Bad deviceid"

    if not token in device.tokens:
        return "Bad token"

    return device.name

@api.route('/api/post/log/<id>/<token>', methods=['POST'])
def postLog(id, token):
    deviceid = int(id)
    logdata = request.form.get('log')

    if deviceid < 0:
        return "Bad deviceid"

    if len(token) != 32:
        return "Bad token"

    device = Devices.query.filter_by(deviceid=deviceid).first()
    if not device:
        return "Bad deviceid"

    if not token in device.tokens:
        return "Bad token"

    device.timestamp = int(time.time())
    new_log = DevicesLogs(deviceid=deviceid,userid=device.userid,timestamp=int(time.time()),logdata=logdata)
    db.session.add(new_log)
    db.session.commit()

    return "Posted"



def getUser(token):
    user = Clients.query.filter_by(token=token).first()
    if not user:
        return False
    return user

@api.route('/api/client/auth/login', methods=['POST'])
def getLogin():
    email = request.form.get('email')
    password = request.form.get('password')
    devicename = request.form.get('devicename')

    user = User.query.filter_by(email=email).first()

    if not user or not check_password_hash(user.password, password):
        return "Bad auth data"

    timestamp = int(time.time())
    token =''.join(random.choices(string.ascii_uppercase + string.digits, k=64))

    new_auth = Clients(id=user.id, token=token, devicename=devicename, timestamp=timestamp)

    db.session.add(new_auth)
    db.session.commit()

    return token

@api.route('/api/client/check/<token>', methods=['GET'])
def checkToken(token):
    user = getUser(token)

    if not user:
        return "Bad auth data"
    
    return "OK"

@api.route('/api/client/devices/list/<token>', methods=['GET'])
def getDevices(token):
    user = getUser(token)

    if not user:
        return "Bad auth data"

    devices_array = Devices.query.filter_by(userid=user.id).all()
    
    return jsonify(devices_array)

@api.route('/api/client/devices/logs/<token>/<deviceid>', methods=['GET'])
def getLogs(token, deviceid):
    user = getUser(token)

    if not user:
        return "Bad auth data"

    deviceid = int(deviceid)
    device = Devices.query.filter_by(deviceid=deviceid).first()

    if not device or device.userid != user.id:
        return "Bad device"

    devicesLogs_array = DevicesLogs.query.filter_by(deviceid=deviceid).all()[::-1][:50]
    
    return jsonify(devicesLogs_array)