from datetime import datetime
from flask import render_template, Blueprint, redirect, url_for, request, flash
from flask_login import login_user, logout_user, login_required
from werkzeug.security import generate_password_hash, check_password_hash
from DudeWeb import db
from .models import User

settings = Blueprint('settings', __name__)

@settings.route('/settings')
@login_required
def settings():
    return render_template('settings.html')