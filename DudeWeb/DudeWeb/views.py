"""
Routes and views for the flask application.
"""

from datetime import datetime
from flask import render_template, Blueprint
from flask_login import login_required, current_user
from DudeWeb import db

main = Blueprint('main', __name__)

@main.route('/')
def index():
    currentPageTitle = "Index"
    return render_template('index.html', currentPageTitle=currentPageTitle)

@main.route('/profile')
@login_required
def profile():
    currentPageTitle = "Profile"
    return render_template('profile.html', currentPageTitle=currentPageTitle, name=current_user.name)