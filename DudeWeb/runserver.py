"""
This script runs the DudeWeb application using a development server.
"""

from os import environ
from DudeWeb import createApp
# from DudeWeb import db, createApp

# db.create_all(app=createApp())

if __name__ == '__main__':
    HOST = environ.get('SERVER_HOST', '0.0.0.0')
    try:
        PORT = int(environ.get('SERVER_PORT', '5555'))
    except ValueError:
        PORT = 5555
    app = createApp()
    app.run(HOST, PORT)
